#!/usr/bin/env python3

import argparse
import copy
import gzip
import json
import math
import os
import re
import subprocess
import sys
from datetime import datetime, timedelta

__version__ = "1.9.14"


def storcli(*args, json_output=True):
    response = subprocess.run(list(args), shell=False, capture_output=True)
    # Note. Some versions of storcli returns 255 (abnormal termination) for normal termination.
    if response.returncode not in [0, 255]:
        sys.stderr.write(response.stderr.decode() + "\n")
        exit(response.returncode)
    stdout = response.stdout.decode()
    return json.loads(stdout) if json_output else stdout


def size_to_bytes(s):
    # fmt: off
    suffixes = ["b", "kb", "mb", "gb", "tb", "pb"]
    matched = re.match(r'(?P<size>-?\d+(?:\.\d*)?)\s*(?P<unit>[kmgtp]?b)', s, re.IGNORECASE)
    if not matched: return s
    unit_index = suffixes.index(matched.groupdict().get("unit").lower())
    return int(float(matched.groupdict().get("size"))*math.pow(2, 10 * unit_index))


def size_to_bytes(s):
    # fmt: off
    suffixes = ["b", "kb", "mb", "gb", "tb", "pb"]
    matched = re.match(r'(?P<size>-?\d+(?:\.\d*)?)\s*(?P<unit>[kmgtp]?b)', s, re.IGNORECASE)
    if not matched: return s
    unit_index = suffixes.index(matched.groupdict().get("unit").lower())
    return int(float(matched.groupdict().get("size"))*math.pow(2, 10 * unit_index))


def time_to_seconds(s):
    # Note. megaraid_sas, megaraid_sas event time, mpt3sas
    formats = ["%m/%d/%Y, %H:%M:%S", "%a %b %d %H:%M:%S %Y", "%m/%d/%Y %H:%M:%S"]
    for f in formats:
        # fmt: off
        try: return int(datetime.timestamp(datetime.strptime(s, f)))
        except: continue
    raise Exception()


def duration_to_seconds(s):
    # fmt: off
    matched = re.match(r'\s*((?P<days>-?\d+) day[s]?)?\s*((?P<hours>-?\d+) hour[s]?)?\s*((?P<minutes>-?\d+) minute[s]?)?\s*', s, re.IGNORECASE)
    if not matched: return s
    p = {"days": 0, "hours": 0, "minutes": 0, "seconds": 0}
    p = {**p, **{k: int(v) for k, v in matched.groupdict().items() if v}}
    return ((p.get('days') * 24 + p.get('hours')) * 60 + p.get('minutes')) * 60 + p.get('seconds')


def raw(options):
    storcli_path = options.get("storcli_path")
    storcli_basename = os.path.basename(storcli_path)
    # Check if storcli64 is installed.
    if not os.path.isfile(storcli_path):
        sys.stderr.write(f"Cannot find {storcli_basename} executable.\n")
        exit(127)  # Note. OS return code for "command not found"
    # Check if storcli64 is executable.
    if not os.access(storcli_path, os.X_OK):
        sys.stderr.write(f"Cannot execute {storcli_basename}.\n")
        exit(126)  # Note. OS return code for "permission denied"
    # Ensure that the user is able to run storcli64.
    if os.geteuid() != 0:
        sys.stderr.write(f"Cannot execute {storcli_basename} without superuser privilege.\n")  # fmt: skip
        exit(5)
    # Ensure that the host equipped with MegaRAID controllers.
    r = storcli(*[storcli_path, "show", "ctrlcount", "J", "nolog"])
    c = r.get("Controllers", [{}])[0].get("Response Data", {}).get("Controller Count")
    if c is None or int(c) == 0:
        sys.stderr.write(f"Cannot find MegaRAID controllers.\n")
        exit(1)

    event_args = [f"type=latest={options.get('event_count')}"]
    if options.get("event_filter") is not None:
        event_args += [f"filter={options.get('event_filter')}"]

    source = {
        # fmt: off
        "controllers": [storcli_path, "/call", "show", "all", "J", "nolog"],
        "enclosures": [storcli_path, "/call/eall", "show", "all", "J", "nolog"],
        "virtual_drives": [storcli_path, "/call/vall", "show", "all", "J", "nolog"],
        "physical_drives": [
            [storcli_path, "/call/eall/sall", "show", "all", "J", "nolog"],
            [storcli_path, "/call/sall", "show", "all", "J", "nolog"],
        ],
        "scheduled_tasks": [storcli_path, "/call", "show", "cc", "pr", "J", "nolog"],
        "vd_operations": [
            [storcli_path, "/call/vall", "show", "bgi", "J", "nolog"],
            [storcli_path, "/call/vall", "show", "cc", "J", "nolog"],
            [storcli_path, "/call/vall", "show", "erase", "J", "nolog"],
            [storcli_path, "/call/vall", "show", "init", "J", "nolog"],
            [storcli_path, "/call/vall", "show", "migrate", "J", "nolog"],
        ],
        "event_logs": [storcli_path, "/call", "show", "events", *event_args, "nolog", {"json_output": False}]
    }

    raw = {}
    for k, v in source.items():
        v = v if isinstance(v[0], list) else [v]
        fn = lambda f, e: f(*e) if not isinstance(e[-1], dict) else f(*e[:-1], **e[-1])
        raw[k] = [fn(storcli, e) for e in v]
        raw[k] = raw[k][0] if len(raw[k]) == 1 else raw[k]
    return raw


def read(options):
    sample_path = options.get("read_raw")
    abs_sample_path = os.path.abspath(sample_path)
    # Check if the sample file exists.
    if not os.path.isfile(abs_sample_path):
        sys.stderr.write(f"Cannot find {sample_path}.\n")
        exit(127)  # Note. OS return code for "command not found"
    # Check if the sample file is readable.
    if not os.access(abs_sample_path, os.R_OK):
        sys.stderr.write(f"Cannot read {sample_path}.\n")
        exit(126)  # Note. OS return code for "permission denied"

    with gzip.open(abs_sample_path, "rb") as stream:
        return json.loads(stream.read())


def dump(options):
    sample_path = options.get("dump_raw")
    abs_sample_path = os.path.abspath(sample_path)
    # Check if the sample file is writable.
    if not os.access(os.path.dirname(abs_sample_path), os.W_OK):
        sys.stderr.write(f"Cannot write {sample_path}.\n")
        exit(126)  # Note. OS return code for "permission denied"

    data = raw(options)
    abs_sample_path += "" if abs_sample_path.endswith(".gz") else ".gz"
    with gzip.open(abs_sample_path, "wb") as stream:
        stream.write(str.encode(json.dumps(data, separators=(",", ":"))))


def collect(raw):
    controllers = collect_controllers(raw.get("controllers"))
    enclosures = collect_enclosures(raw.get("enclosures"))
    virtual_drives = collect_virtual_drives(raw.get("virtual_drives"))
    physical_drives = collect_physical_drives(
        raw.get("controllers"),
        raw.get("virtual_drives"),
        raw.get("physical_drives"),
    )
    scheduled_tasks = collect_scheduled_tasks(
        raw.get("controllers"),
        controllers,
        raw.get("scheduled_tasks"),
    )
    vd_operations = collect_virtual_drive_operations(
        raw.get("controllers"),
        raw.get("vd_operations"),
    )
    event_logs = collect_event_logs(controllers, raw.get("event_logs"))
    return {
        "controllers": controllers,
        "enclosures": enclosures,
        "virtual_drives": virtual_drives,
        "physical_drives": physical_drives,
        "scheduled_tasks": scheduled_tasks,
        "vd_operations": vd_operations,
        "event_logs": event_logs,
    }


def collect_controllers(ctrl_info):
    o = []
    for c in ctrl_info.get("Controllers"):
        if c.get("Command Status").get("Status") == "Success":
            controller = c.get("Command Status").get("Controller")
            r = c.get("Response Data")
            z = {"controller": controller}
            #
            s = r.get("Basics")
            assert controller == s.get("Controller")
            z["model"] = s.get("Model")
            z["serial"] = s.get("Serial Number")
            z["status"] = r.get("Status").get("Controller Status")
            st = s.get("Current System Date/time")
            ct = s.get("Current Controller Date/Time")
            z["system_time"] = None if st is None else time_to_seconds(st)
            z["controller_time"] = None if ct is None else time_to_seconds(ct)
            both_available = z.get("system_time") is not None and z.get("controller_time") is not None  # fmt: skip
            z["time_diff"] = z.get("system_time") - z.get("controller_time") if both_available else None  # fmt: skip
            #
            s = r.get("Version")
            z["firmware"] = s.get("Firmware Version")
            z["driver"] = s.get("Driver Name")
            #
            s = r.get("HwCfg")
            t = s.get("On Board Memory Size")
            z["memory"] = None if t is None else size_to_bytes(t)
            t = s.get("ROC temperature(Degree Celsius)")
            z["temperature"] = t or s.get("ROC temperature(Degree Celcius)")
            z["frontend_ports"] = s.get("Front End Port Count")
            z["backend_ports"] = s.get("Backend Port Count")
            z["battery"] = s.get("BBU")
            z["alarm"] = s.get("Alarm")
            #
            o += [z]
    return o


def collect_enclosures(encl_info):
    o = []
    for c in encl_info.get("Controllers"):
        if c.get("Command Status").get("Status") == "Success":
            controller = c.get("Command Status").get("Controller")
            r = c.get("Response Data", {})
            for k in r.keys():
                z = {"controller": controller}
                #
                s = r.get(k).get("Information")
                z["enclosure"] = s.get("Device ID")
                z["enclosure_type"] = s.get("Enclosure Type")
                z["status"] = s.get("Status")
                z["device_type"] = s.get("Device Type")
                t = s.get("Enclosure Serial Number", "N/A").strip()
                z["serial"] = None if t == "N/A" or t == "" else t
                #
                s = r.get(k).get("Inquiry Data")
                z["vendor"] = s.get("Vendor Identification")
                z["product"] = s.get("Product Identification")
                t = s.get("Product Revision Level", "N/A")
                z["revision"] = None if t == "N/A" else t
                #
                s = r.get(k).get("Properties")[0]  # Why is it a list?
                z["slots"] = int(s.get("Slots"))
                z["physical_drives"] = int(s.get("PD"))
                #
                o += [z]
    return o


def collect_virtual_drives(vd_info):
    o = []
    for c in vd_info.get("Controllers"):
        if c.get("Command Status").get("Status") == "Success":
            controller = c.get("Command Status").get("Controller")
            r = c.get("Response Data", {})
            for k in filter(lambda e: e.startswith(f"/c{controller}"), r.keys()):
                z = {"controller": controller}
                assert k.startswith(f"/c{controller}")
                virtual_drive = int(k[len(f"/c{controller}/v") :])
                #
                s = r.get(f"/c{controller}/v{virtual_drive}")[0]  # Why is it a list?
                dg, vd = s.get("DG/VD", "-1/-1").split("/")
                dg, vd = int(dg), int(vd)
                assert virtual_drive == int(vd)
                z["dg"] = dg
                z["vd"] = vd
                z["type"] = s.get("TYPE")
                z["state"] = s.get("State")
                z["cache"] = s.get("Cache")
                t = s.get("Size")
                z["size"] = None if t is None else size_to_bytes(t)
                z["name"] = s.get("Name")
                z["drives"] = len(r.get(f"PDs for VD {vd}"))
                #
                s = r.get(f"VD{vd} Properties")
                t = s.get("Strip Size")
                z["stripe"] = None if t is None else size_to_bytes(t)
                z["disk_cache"] = s.get("Disk Cache Policy")
                z["drive_name"] = s.get("OS Drive Name")
                t = s.get("Number of Drives Per Span", "N/A")
                z["per_span_count"] = None if t == "N/A" else t
                #
                o += [z]
    return o


def collect_physical_drives(ctrl_info, vd_info, pd_info):
    m = {}  # Note. CID, DID (device) to VID (controller, virtual drive)
    for c in vd_info.get("Controllers"):
        if c.get("Command Status").get("Status") == "Success":
            controller = c.get("Command Status").get("Controller")
            r = c.get("Response Data", {})
            for k in filter(lambda e: e.startswith("PDs for VD "), r.keys()):
                vid = int(k[len("PDs for VD ") :].strip())
                for d in r.get(k):
                    m[f"{controller},{d.get('DID')}"] = vid
    n = {}  # Note. CID, DID (device) to DG, span, row (A.K.A topology)
    for c in ctrl_info.get("Controllers"):
        if c.get("Command Status").get("Status") == "Success":
            controller = c.get("Command Status").get("Controller")
            for k in c.get("Response Data", {}).get("TOPOLOGY", []):
                if k.get("DID") != "-":
                    dg = int(k.get("DG"))
                    span = None if k.get("Arr") == "-" else int(k.get("Arr"))
                    row = None if k.get("Row") == "-" else int(k.get("Row"))
                    n[f"{controller},{k.get('DID')}"] = [dg, span, row]

    o = []
    for pd in pd_info:
        for c in pd.get("Controllers"):
            if c.get("Command Status").get("Status") == "Success":
                controller = c.get("Command Status").get("Controller")
                r = c.get("Response Data", {})
                for k in filter(lambda e: not e.endswith(" Information"), r.keys()):
                    z = {"controller": controller}
                    assert k.startswith(f"Drive /c{controller}")
                    #
                    s = r.get(k)[0]  # Why is it a list?
                    t = s.get("EID:Slt").strip()
                    enclosure, slot = t.split(":")
                    z["enclosure"] = None if enclosure.strip() == "" else int(enclosure)
                    z["slot"] = int(slot)
                    did = s.get("DID")
                    z["did"] = did
                    z["vd"] = m.get(f"{controller},{did}")
                    t = s.get("State", "-")
                    z["state"] = None if t == "-" else t
                    t = s.get("Size")
                    z["size"] = None if t is None else size_to_bytes(t)
                    z["interface"] = s.get("Intf")
                    z["media"] = s.get("Med")
                    t = s.get("Sp", "-")
                    z["spun"] = None if t == "-" else t
                    t = s.get("Type", "-")
                    z["type"] = None if t == "-" else t
                    z["dg"], z["span"], z["row"] = n.get(f"{controller},{did}", [None, None, None])  # fmt: skip
                    #
                    s = r.get(f"{k} - Detailed Information").get(f"{k} State")
                    t = s.get("Shield Counter", "N/A")
                    z["shield_counter"] = None if t == "N/A" else int(t)
                    t = s.get("Media Error Count", "N/A")
                    z["media_error_count"] = None if t == "N/A" else int(t)
                    t = s.get("Other Error Count", "N/A")
                    z["other_error_count"] = None if t == "N/A" else int(t)
                    t = s.get("Predictive Failure Count", "N/A")
                    z["predictive_failure_count"] = None if t == "N/A" else int(t)
                    t = s.get("S.M.A.R.T alert flagged by drive", "N/A")
                    # Note. Dots in the string in "S.M.A.R.T", do not use pydash.get.
                    z["smart_alerted"] = None if t == "N/A" else t == "Yes"
                    t = s.get("Drive Temperature", "").strip().split("C")[0]
                    z["temperature"] = None if t == "" else int(t)
                    #
                    s = r.get(f"{k} - Detailed Information").get(f"{k} Device attributes")  # fmt: skip
                    z["manufacturer"] = s.get("Manufacturer Id").strip()
                    z["model"] = s.get("Model Number").strip()
                    z["serial"] = s.get("SN").strip()
                    t = s.get("Device Speed", "Unknown").replace("Gb/s", "")
                    z["drive_speed"] = None if t == "Unknown" else float(t)
                    t = s.get("Link Speed", "Unknown").replace("Gb/s", "")
                    z["link_speed"] = None if t == "Unknown" else float(t)
                    #
                    s = r.get(f"{k} - Detailed Information").get(f"{k} Policies/Settings")  # fmt: skip
                    t = s.get("Commissioned Spare", "N/A")
                    z["commissioned_spare"] = None if t == "N/A" else t == "Yes"
                    t = s.get("Emergency Spare", "N/A")
                    z["emergency_spare"] = None if t == "N/A" else t == "Yes"
                    #
                    o += [z]
    return o


def collect_scheduled_tasks(ctrl_info, controllers, task_info):
    m = {}  # Note. CID, policy to policy rates
    for c in ctrl_info.get("Controllers"):
        if c.get("Command Status").get("Status") == "Success":
            controller = c.get("Command Status").get("Controller")
            r = c.get("Response Data", {}).get("Policies", {}).get("Policies Table", [])
            pick = lambda e: next(filter(lambda x: x.get("Policy") == e, r)) or {}
            pr = int(pick("PR Rate").get("Current").replace(" %", ""))
            cc = int(pick("Check Consistency Rate").get("Current").replace(" %", ""))
            m[f"{controller},cc"] = cc
            m[f"{controller},pr"] = pr

    o = []
    for c in task_info.get("Controllers"):
        if c.get("Command Status").get("Status") == "Success":
            controller = c.get("Command Status").get("Controller")
            r = c.get("Response Data", {}).get("Controller Properties", [])
            r = {e.get("Ctrl_Prop"): e.get("Value") for e in r}

            t = next(filter(lambda e: e.get("controller") == controller, controllers))
            time_diff = t.get("time_diff")
            if r:
                z = {"controller": controller}
                z["name"] = "Consistency check"
                z["rate"] = m.get(f"{controller},cc")
                z["mode"] = r.get("CC Operation Mode")
                z["state"] = r.get("CC Current State")
                t = r.get("CC Next Starttime")
                z["next_controller_time"] = None if t is None else time_to_seconds(t)
                z["next_system_time"] = None if t is None else z.get("next_controller_time") + time_diff  # fmt: skip
                t = r.get("CC Execution Delay", "").replace(" hours", "")
                z["delay"] = None if t == "" else int(t) * 60 * 60  # in seconds
                # Note. the number of virtual drives done by consistency check task
                t = r.get("CC Number of VD completed")
                z["drives_done"] = None if t is None or z.get("state") != "Active" else int(t)  # fmt: skip
                o += [z]  # fmt: skip
                #
                z = {"controller": controller}
                z["name"] = "Patrol read"
                z["rate"] = m.get(f"{controller},pr")
                z["mode"] = r.get("PR Mode")
                state = r.get("PR Current State")
                t = r.get("PR Next Start time")
                z["next_controller_time"] = None if t is None else time_to_seconds(t)
                z["next_system_time"] = None if t is None else z.get("next_controller_time") + time_diff  # fmt: skip
                t = r.get("PR Execution Delay", "").replace(" hours", "")
                z["delay"] = None if t == "" else int(t) * 60 * 60  # in seconds
                # Note. the number of physical drives done by patrol read task
                z["drives_done"] = int(state[len("Active ") :]) if state.startswith("Active") else None  # fmt: skip
                z["state"] = "Active" if state.startswith("Active") else state
                o += [z]
    return o


def collect_virtual_drive_operations(ctrl_info, op_info):
    m = {}  # Note. CID, policy to policy rates
    for c in ctrl_info.get("Controllers"):
        if c.get("Command Status").get("Status") == "Success":
            controller = c.get("Command Status").get("Controller")
            r = c.get("Response Data", {}).get("Policies", {}).get("Policies Table", [])
            pick = lambda e: next(filter(lambda x: x.get("Policy") == e, r)) or {}
            cc = int(pick("Check Consistency Rate").get("Current").replace(" %", ""))
            bgi = int(pick("BGI Rate").get("Current").replace(" %", ""))
            m[f"{controller},CC"] = cc
            m[f"{controller},BGI"] = bgi

    o = []
    for op in op_info:
        for c in op.get("Controllers"):
            if c.get("Command Status").get("Status") == "Success":
                controller = c.get("Command Status").get("Controller")
                for r in c.get("Response Data", {}).get("VD Operation Status", {}):
                    z = {"controller": controller}
                    z["virtual_drive"] = r.get("VD")
                    z["name"] = r.get("Operation")
                    z["rate"] = m.get(f"{controller},{z.get('name')}")
                    z["status"] = r.get("Status")
                    t = r.get("Progress%", "-")
                    z["progress"] = None if t == "-" else t
                    t = r.get("Estimated Time Left", "-")
                    z["time_left"] = None if t == "-" else duration_to_seconds(t)
                    o += [z]
    return o


def collect_event_logs(controllers, log_info):
    event_severity = {
        "-1": "progress",  # Progress message. No user action is necessary.
        "0": "info",  # Informational message. No user action is necessary.
        "1": "warning",  # Some component might be close to a failure point.
        "2": "critical",  # A component has failed, but the system has not lost data.
        "3": "fatal",  # A component has failed, and data loss has occurred or will occur.
        "4": "fault",  # The I/O Unit faulted due to a catastrophic error.
    }
    event_kv = {
        "seqNum:": "sequence",
        "Time:": "controller_time",
        "Seconds since last reboot:": "since",
        "Code:": "code",
        "Class:": "class",
        "Locale:": "locale",
        "Event Description:": "description",
    }
    event_keys = event_kv.keys()
    event_start_prefix = "seqNum: "
    controller_start_prefix = "CLI Version = "

    o = []
    state, events, z = None, [], {}
    for line in log_info.splitlines():
        line = line.strip()
        # fmt: off
        if line == "": continue  # skip empty lines
        event_start = line.startswith(event_start_prefix)
        controller_start = line.startswith(controller_start_prefix)
        if event_start or controller_start:
            if z: events += [z]
            z = {}
            state = "event" if event_start else "controller"
        if state == "event":
            k = next(filter(lambda e: line.startswith(e), event_keys), None)
            if k: z[event_kv.get(k)] = line[len(k) :].strip()
            #
            if k == "seqNum:": z["sequence"] = int(z.get("sequence"), 0)
            elif k == "Time:":
                z["controller_time"] = time_to_seconds(z["controller_time"].replace("  ", " 0"))
            elif k == "Seconds since last reboot:":
                if z.get("since") is not None:
                    z["since"] = int(z["since"])
                    z["controller_time"] = last_reboot + z["since"]
            elif k == "Class:":
                z["severity"] = event_severity.get(z["class"], "unknown")
            elif k == "Event Description:" and z["description"].startswith("Time established as"):
                # Time established as 03/17/25  0:40:05; (48 seconds since power on)
                since_seconds = int(z["description"].split("(")[1].split(" seconds")[0])
                last_reboot = z["controller_time"] - since_seconds
        elif state == "controller" and line.startswith("Controller = "):
            controller = int(line[len("Controller = ") :].strip())
            # Get the time difference of the controller.
            t = next(filter(lambda e: e.get("controller") == controller, controllers))
            time_diff = t.get("time_diff")
            for e in events:
                t = e.get("controller_time")
                e["system_time"] = None if t is None else t + time_diff
            o += [{"controller": controller, **e} for e in events]
            events = []
        # fmt: on
    return o


def translate(options, collected):
    def trans_value(s, dictionary):
        return None if s is None else dictionary.get(s, s)

    def tag_value(s, dictionary):
        # fmt: off
        # Note. To tag a value computed by more than one fields, do so in render function.
        if s is None: return None
        if options.get("no_color"): return s
        tag = dictionary.get(s)
        return s if tag is None else f"<{tag}>{s}</{tag}>"

    copied = copy.deepcopy(collected)

    for d in copied.get("controllers"):
        d["status"] = trans_value(d["status"], {"Needs Attention": "Attention"})
        d["alarm"] = trans_value(d["alarm"], {"Disable": "Disabled"})
        d["status"] = tag_value(d["status"], {
            "Attention": "warning",
            "Degraded": "warning",
            "Failed": "fatal",
        })  # fmt: skip
        d["alarm"] = tag_value(d["alarm"], {"Disabled": "warning"})

    for d in copied.get("virtual_drives"):
        d["state"] = trans_value(d["state"], {"Dgrd": "Degraded", "Optl": "Optimal"})
        d["disk_cache"] = trans_value(d["disk_cache"], {"Disk's Default": "Default"})
        d["state"] = tag_value(d["state"], {"Degraded": "warning"})

    for d in copied.get("physical_drives"):
        d["state"] = trans_value(d["state"], {
            "Cpybck": "Copy back",
            "Offln": "Offline",
            "Onln": "Online",
            "Rbld": "Rebuild",
            "Sntze": "Sanitize",  # Wipe out disk data
            "CBShld": "CBS",  # copy back shielded
            "CFShld": "CFS",  # configured shielded
            "DHS": "DHS",  # dedicated hot spare
            "GHS": "GHS",  # global hos spare
            "HSPShld": "HSS",  # hot spare shielded
            "UBad": "UB",  # unconfigured bad
            "UBUnsp": "UBU",  # unconfigured bad unsupported
            "UGood": "UG",  # unconfigured good
            "UGShld": "UGS",  # unconfigured good shielded
            "UGUnsp": "UGU",  # Unconfigured good unsupported
        })  # fmt: skip
        d["spun"] = trans_value(d["spun"], {"U": "Up", "D": "Down"})
        d["type"] = trans_value(d["type"], {"F": "Foreign", "T": "Transition"})
        d["smart_alerted"] = trans_value(d["smart_alerted"], {True: "Predicted", False: None})  # fmt: skip
        d["state"] = tag_value(d["state"], {
            "Rebuild": "info",
            "Offline": "warning",
            "Sanitize": "warning",
            "CBS": "info",
            "CFS": "info",
            "HSS": "info",
            "UGS": "info",
            "UB": "error",
            "UBU": "error",
        })  # fmt: skip
        d["type"] = tag_value(d["type"], {"Foreign": "info"})
        d["smart_alerted"] = tag_value(d["smart_alerted"], {"Predicted": "warning"})

    for d in copied.get("scheduled_tasks"):
        d["mode"] = trans_value(d["mode"], {"Disable": "Disabled"})
        d["mode"] = tag_value(d["mode"], {"Disabled": "warning"})
        d["state"] = tag_value(d["state"], {"Active": "info", "Paused": "info"})

    for d in copied.get("vd_operations"):
        d["name"] = trans_value(d["name"], {
            "BGI": "Background initialization",
            "CC": "Consistency check",
            "ERASE": "Erasure",
            "INIT": "Initialization",
            "Migrate": "Migration",
        })  # fmt: skip
        d["status"] = trans_value(d["status"], {
            "Not in progress": "Inactive",
            "In progress": "Active",
        })  # fmt: skip
        d["status"] = tag_value(d["status"], {"Active": "info"})

    for d in copied.get("event_logs"):
        d["severity"] = tag_value(d["severity"], {
            "info": "info",
            "warning": "warning",
            "critical": "error",
            "fatal": "fatal",
            "fault": "fatal",
        })  # fmt: skip

    return copied


def render(options, collected):
    SEPARATOR = "{{separator}}"
    no_data = "No data available"

    def colorize(s, strip=False):
        tags = {
            "bold": "\033[1m",  # bold
            "info": "\033[32m\033[1m",  # green
            "warning": "\033[33m\033[1m",  # yellow
            "error": "\033[31m\033[1m",  # red
            "fatal": "\033[31m\033[7m",  # red, inverse
            "reset": "\033[0m",  # reset
        }
        tagged = re.compile(r"<(\S+)>(.*)</\1>")
        matched = tagged.match(s)
        if matched and matched.groups:
            tag, body = matched.group(1), matched.group(2)
            open, close = ("", "") if strip else (tags.get(tag.lower(), ""), tags.get("reset", ""))  # fmt: skip
            # Note. s could be justified. So f"{tags.get(tag)}{body}{...}" will break column width.
            return s.replace(f"<{tag}>", open).replace(f"</{tag}>", close)
        return s

    def rows(data, cols):
        return [[c(e) if c(e) != "None" else "" for c in cols] for e in data]

    def maxwidth(headers, data):
        width = [len(colorize(e, True)) for e in headers]
        for d in data:
            if d != SEPARATOR:
                width = [max(width[i], len(colorize(e, True))) for i, e in enumerate(d)]
        return width

    def separate(data, key_index=0):
        separated = []
        for i in range(len(data)):
            if i >= 1 and data[i - 1][key_index] != data[i][key_index]:
                separated += [SEPARATOR]
            separated += [data[i]]
        return separated

    def tabulate(title, data, aligns, header, footer=None):
        # fmt: off
        padding = lambda e, i: width[i] - len(colorize(e, True))
        justify = lambda e, i: e + " " * padding(e, i) if aligns[i] == "l" else " " * padding(e, i) + e
        format = lambda r: "|" + "".join(f" {colorize(justify(e, i))} |" for i, e in enumerate(r))
        # fmt: on

        width = maxwidth(header, data)
        separator = "+" + "".join([f"-{'-'*e}-+" for e in width])
        half_len = (len(separator) - (4 + len(no_data))) / 2
        empty = f"| {' '*math.floor(half_len)}{no_data}{' '*math.ceil(half_len)} |"
        rows = [separator if e == SEPARATOR else format(e) for e in data] if data else [empty]  # fmt: skip

        if not options.get("no_color"):
            title = colorize(f"<bold>{title.upper()}</bold>")
        footer = [] if footer is None or not options.get("footers") else footer
        return "\n".join(
            [title, separator, format(header), separator] + rows + [separator] + footer
        )

    def sort_data(d, index=0):
        s = sorted(d, key=lambda e: e[index])
        nonempty = filter(lambda e: e[index] != "", s)
        empty = filter(lambda e: e[index] == "", s)
        return list(nonempty) + list(empty)

    def bytes_to_size(s):
        suffixes = ["B", "KiB", "MiB", "GiB", "TiB", "PiB"]
        digits = math.floor(math.log10(max(1, s))) + 1
        unit_index = int((digits - 0.1) // 3)
        return "{:.2f}".format(s / pow(2, 10 * unit_index)) + " " + suffixes[unit_index]

    def seconds_to_time(s):
        return datetime.strftime(datetime.fromtimestamp(s), "%a %Y-%m-%d %H:%M:%S")

    def seconds_to_duration(s):
        duration = str(timedelta(seconds=s))
        if "day" in duration:
            days, rest = duration.split(",")
            return f"{days}, {rest.strip().rjust(8, '0')}"
        return duration.rjust(8, "0")

    def render_percent(e):
        return None if e is None else f"{e}%"

    def render_temperature(e):  # Celsius
        return None if e is None else f"{e}C"

    def render_size(e):
        return None if e is None else bytes_to_size(e)

    def render_time(e):  # seconds to datetime
        return None if e is None else seconds_to_time(e)

    def render_duration(e):  # seconds to durations
        return None if e is None else seconds_to_duration(e)

    def render_hours(e):  # seconds in hours
        return None if e is None else f"{int(e / 60 / 60)} hours"

    def render_speed(e):  # Gbps
        return None if e is None else f"{e}Gbps"

    def render_truncated(e, maxlen=90):
        return e if len(str(e)) <= maxlen else e[: maxlen - 3] + "..."

    o = []

    title = "Controllers"
    data = rows(collected.get("controllers"), [
        lambda e: f"/c{e.get('controller')}",
        lambda e: f"{e.get('model')} ({e.get('serial')}, {e.get('firmware')})",
        lambda e: f"{e.get('status')}",
        lambda e: f"{render_size(e.get('memory'))}",
        lambda e: f"{render_temperature(e.get('temperature'))}",
        lambda e: f"{e.get('battery')}",
        lambda e: f"{e.get('alarm')}",
        lambda e: f"{render_time(e.get('system_time'))}",
        lambda e: f"{render_duration(e.get('time_diff'))}"
    ])  # fmt: skip
    aligns = ["l", "l", "l", "r", "r", "l", "l", "l", "r"]
    header = [
        "CID", "Controller (serial, firmware)", "Status", "RAM", "Tpr", "Battery",
        "Alarm", "Clock (system time)", "TimeDiff",
    ]  # fmt: skip
    footer = [
        "* TimeDiff is the difference between system time and controller time if any.",
    ]
    o += [tabulate(title, data, aligns, header, footer)]

    title = "Enclosures"
    data = rows(collected.get("enclosures"), [
        lambda e: f"/c{e.get('controller')}/e{e.get('enclosure')}",
        lambda e: f"{e.get('vendor')} {e.get('product')} ({e.get('serial')}, {e.get('revision')})",  # fmt: skip
        lambda e: f"{e.get('status')}",
        lambda e: f"{e.get('device_type')}",
        lambda e: f"{e.get('slots')}",
        lambda e: f"{e.get('physical_drives')}",
    ])  # fmt: skip
    aligns = ["l", "l", "l", "l", "r", "r"]
    header = ["EID", "Enclosure (serial, rev.)", "Status", "Type", "#Slt", "#Drv"]
    o += [tabulate(title, data, aligns, header)]

    def render_cache(e):
        return e.replace("R", "R,").replace("C", ",C").replace("D", ",D")

    title = "Virtual drives"
    data = rows(collected.get("virtual_drives"), [
        lambda e: f"/c{e.get('controller')}/v{e.get('vd')}",
        lambda e: f"{e.get('type')}",
        lambda e: f"{e.get('state')}",
        lambda e: f"{render_size(e.get('size'))}",
        lambda e: f"{e.get('drives')}",
        lambda e: f"{render_size(e.get('stripe'))}",
        lambda e: f"{render_cache(e.get('cache'))}",
        lambda e: f"{e.get('disk_cache')}",
        lambda e: f"{e.get('name')}",
        lambda e: f"{e.get('drive_name')}",
    ])  # fmt: skip
    aligns = ["l", "l", "l", "r", "r", "r", "l", "l", "l", "l"]
    header = [
        "VID", "Type", "Status", "Size", "#Drv", "StrpSz", "CacheFlg", "DskCache",
        "Name", "Path"
    ]  # fmt: skip
    footer = [
        "* R=Read Ahead Always,NR=No Read Ahead/WB=Write Back,AWB=Always Write Back,WT=Write Through/C=Cached IO,D=Direct IO",
    ]
    o += [tabulate(title, data, aligns, header, footer)]

    def render_vid(e):
        if e.get("vd") is None:
            return None
        return f"/c{e.get('controller')}/v{e.get('vd')}"

    def render_sid(e):
        # fmt: off
        if e.get("enclosure") is not None:
            return f"/c{e.get('controller')}/e{e.get('enclosure')}/s{e.get('slot')}"
        return None if e.get("slot") is None else f"/c{e.get('controller')}/s{e.get('slot')}"

    def render_model(e):
        prefix = "" if e.get("manufacturer") == "ATA" else f"{e.get('manufacturer')} "
        return f"{prefix}{e.get('model').replace('  ', ' ')} ({e.get('serial')})"

    def render_position(e):
        # fmt: off
        if e.get("dg") is None: return None
        if e.get("span") is None: return f"{str(e.get('dg')).rjust(2)}"
        return f"{str(e.get('dg')).rjust(2)}:{str(e.get('span')).rjust(2)}:{str(e.get('row')).rjust(2)}"

    title = "Physical drives"
    data = rows(collected.get("physical_drives"), [
        lambda e: f"{render_vid(e)}",
        lambda e: f"{render_sid(e)}",
        lambda e: f"{render_model(e)}",
        lambda e: f"{e.get('state')}",
        lambda e: f"{e.get('interface')}",
        lambda e: f"{e.get('media')}",
        lambda e: f"{render_size(e.get('size'))}",
        lambda e: f"{e.get('spun')}",
        lambda e: f"{render_temperature(e.get('temperature'))}",
        lambda e: f"{render_speed(e.get('drive_speed'))}",
        lambda e: f"{render_speed(e.get('link_speed'))}",
        lambda e: "0x{:02x}".format(e.get('did')),
        lambda e: f"{render_position(e)}",
        lambda e: f"{e.get('type')}",
        lambda e: f"{e.get('smart_alerted')}"
    ])  # fmt: skip
    # Note. Sort based on VID and separate based on VID
    data = separate(sort_data(data))
    aligns = ["l", "l", "l", "l", "l", "l", "r", "l", "r", "r", "r", "r", "l", "l", "l"]
    header = [
        "VID", "SID", "Drive (serial)", "Status", "Intf", "Med", "Size", "Spun", "Tpr",
        "DevcSpd", "LinkSpd", "DID", "DG:SP:RW", "Type", "Fail"
    ]  # fmt: skip
    footer = [
        "* CBS=Copy Back Shielded,CFS=Configured Shielded,DHS=Dedicated Hot Spare,GHS=Global Hot Spare,HSS=Hot Spare Shielded,UB=Unconfigured Bad",
        "  UBU=Unconfigured Bad (Unsupported),UG=Unconfigured Good,UGS=Unconfigured Good Shielded,UGU=Unconfigured Good (Unsupported)",
    ]
    o += [tabulate(title, data, aligns, header, footer)]

    def render_drives_done(e):
        # fmt: off
        if "Active" not in e.get("state"): return None
        drive = "VD" if e.get("name") == "Consistency check" else "PD"
        return f"{e.get('drives_done')} {drive}s done"

    title = "Controller tasks"
    data = rows(collected.get("scheduled_tasks"), [
        lambda e: f"/c{e.get('controller')}",
        lambda e: f"{e.get('name')}",
        lambda e: f"{e.get('mode')}",
        lambda e: f"{render_hours(e.get('delay'))}",
        lambda e: f"{render_percent(e.get('rate'))}",
        lambda e: f"{render_time(e.get('next_system_time'))}",
        lambda e: f"{e.get('state')}",
        lambda e: f"{render_drives_done(e)}",
    ])  # fmt: skip
    aligns = ["l", "l", "l", "r", "r", "l", "l", "r"]
    header = [
        "CID", "Task", "Mode", "Delay", "Rate", "Next run (system time)", "Status",
        "Progress"
    ]  # fmt: skip
    o += [tabulate(title, data, aligns, header)]

    def render_progress(e):
        # fmt: off
        if e.get("status") == "Inactive": return None
        return f"{e.get('progress')}% ({seconds_to_duration(e.get('time_left'))} left)"

    title = "Virtual drive operations"
    data = rows(collected.get("vd_operations"), [
        lambda e: f"/c{e.get('controller')}/v{e.get('virtual_drive')}",
        lambda e: f"{e.get('name')}",
        lambda e: f"{render_percent(e.get('rate'))}",
        lambda e: f"{e.get('status')}",
        lambda e: f"{render_progress(e)}",
    ])  # fmt: skip
    # Note. Sort based on VID and filter out inactive operations.
    data = list(filter(lambda e: e[3] != "Inactive", sort_data(data)))
    if len(data) >= 1:
        aligns = ["l", "l", "r", "l", "r"]
        header = ["VID", "Operation", "Rate", "Status", "Progress"]
        o += [tabulate(title, data, aligns, header)]

    def render_event_time(e):
        # fmt: skip
        if e.get("system_time"):
            return seconds_to_time(e.get("system_time"))
        return f"{e.get('since')} secs since reboot"

    title = "Event logs"
    data = rows(collected.get("event_logs"), [
        lambda e: f"/c{e.get('controller')}",
        lambda e: f"{e.get('sequence')}",
        lambda e: f"{render_event_time(e)}",
        lambda e: f"{render_truncated(e.get('description'))}",
        lambda e: f"{e.get('severity').upper()}",
    ])  # fmt: skip
    data = separate(data)  # Note. Separate based on CID.
    aligns = ["l", "r", "l", "l", "l"]
    header = ["CID", "Index", "Occurred (system time)", "Log", "Severity"]
    # @see https://techdocs.broadcom.com/content/dam/broadcom/techdocs/data-center-solutions/tools/generated-pdfs/12Gbs-MegaRAID-Tri-Mode-Software.pdf
    footer = [
        "* CRITICAL=error without data loss,FATAL=error with possible data loss,FAULT=catastrophic hardware failure",
    ]
    o += [tabulate(title, data, aligns, header, footer)]

    return "\n".join(o)


def parse():
    parser = argparse.ArgumentParser(
        description="Print MegaRAID controller and drive information",
        formatter_class=lambda prog: argparse.HelpFormatter(
            prog, max_help_position=100
        ),
    )
    # fmt: off
    parser.add_argument("--storcli-path", metavar="<string>", type=str, default="/opt/MegaRAID/storcli/storcli64", help="specify storcli executable path")
    parser.add_argument("--footers", action="store_true", default=False, help="show footers")
    parser.add_argument("--event-count", metavar="<number>", type=int, default=5, help="specify the number of events (default: 5)")
    parser.add_argument("--event-filter", choices=["info", "warning", "critical", "fatal"], default=None, help="specify the severity of events")
    parser.add_argument("--json", action="store_true", default=False, help="print output in JSON format")
    parser.add_argument("--no-color", action="store_true", default=False, help="print in monochrome")
    parser.add_argument("--dump-raw", metavar="<string>", type=str, help="specify file path to dump raw JSON data (for debugging purpose)")
    parser.add_argument("--read-raw", metavar="<string>", type=str, help="specify file path to read raw JSON data (for debugging purpose)")
    parser.add_argument("--version", action="version", version="%(prog)s " + __version__)
    # fmt: on
    return parser


if __name__ == "__main__":
    options = vars(parse().parse_args())
    if options.get("dump_raw"):
        dump(options)
        exit(0)
    data = read(options) if options.get("read_raw") else raw(options)
    collected = collect(data)
    if options.get("json"):
        sys.stdout.write(json.dumps(collected) + "\n")
    else:
        translated = translate(options, collected)
        rendered = render(options, translated)
        sys.stdout.write(rendered + "\n")
    sys.stdout.flush()
