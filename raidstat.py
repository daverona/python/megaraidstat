#!/usr/bin/env python3

import argparse
import copy
import gzip
import inspect
import json
import os
import re
import subprocess
import sys
from datetime import datetime

__version__ = "1.9.0"


def storcli(*args, json_output=True):
    response = subprocess.run(list(args), shell=False, capture_output=True)
    # Note. Some versions of storcli returns 255 (abnormal termination) for normal termination.
    if response.returncode not in [0, 255]:
        sys.stderr.write(response.stderr.decode() + "\n")
        exit(response.returncode)
    stdout = response.stdout.decode()
    return json.loads(stdout) if json_output else stdout


def dictify(*variables):
    current_frame = inspect.currentframe()
    caller_frame = inspect.getouterframes(current_frame)[1]
    local_vars = caller_frame.frame.f_locals
    return {k: v for k, v in local_vars.items() if v in variables}


def format_time(s, format=True):
    formats = [
        "%m/%d/%Y, %H:%M:%S",  # megaraid_sas:
        "%a %b %d %H:%M:%S %Y",  # megaraid_sas: event time
        "%m/%d/%Y %H:%M:%S",  # mpt3sas:
    ]
    for f in formats:
        # fmt: off
        try:
            t = datetime.strptime(s, f)
            if not format: return t
            return datetime.strftime(t, "%a %Y-%m-%d %H:%M:%S")
        except: continue
    return s


def format_duration(s):
    # fmt: off
    timed = re.compile(r"\s*((?P<d>-?\d+) day[s]?)?\s*((?P<h>-?\d+) hour[s]?)?\s*((?P<m>-?\d+) minute[s]?)?\s*")
    matched = timed.match(s, re.IGNORECASE)
    if not matched: return s
    p = {"d": None, "h": 0, "m": 0, "s": 0}
    p = {**p, **{k: int(v) for k, v in matched.groupdict().items() if v}}
    days = "" if not p.get("d") else f'{p.get("d")}d '
    hours = str(p.get('h')).rjust(2, '0')
    minutes = str(p.get('m')).rjust(2, '0')
    seconds = str(p.get('s')).rjust(2, '0')
    return f"{days}{hours}:{minutes}:{seconds}"


def raw(options):
    storcli_path = options.get("storcli_path")
    # Check if storcli64 is installed.
    if not os.path.isfile(storcli_path):
        sys.stderr.write("Cannot find storcli64 executable.\n")
        exit(127)  # Note. OS return code for "command not found"
    # Check if storcli64 is executable.
    if not os.access(storcli_path, os.X_OK):
        sys.stderr.write("Cannot execute storcli64.\n")
        exit(126)  # Note. OS return code for "permission denied"
    if os.geteuid() != 0:
        sys.stderr.write(f"Cannot execute storcli64 without superuser privilege.\n")
        exit(5)

    source = {
        "controllers": [storcli_path, "/call", "show", "all", "J", "nolog"],
        "enclosures": [storcli_path, "/call/eall", "show", "all", "J", "nolog"],
        "virtual_drives": [storcli_path, "/call/vall", "show", "all", "J", "nolog"],
        "physical_drives": [
            [storcli_path, "/call/eall/sall", "show", "all", "J", "nolog"],
            [storcli_path, "/call/sall", "show", "all", "J", "nolog"],
        ],
        "scheduled_tasks": [storcli_path, "/call", "show", "cc", "pr", "J", "nolog"],
        "vd_operations": [
            [storcli_path, "/call/vall", "show", "bgi", "J", "nolog"],
            [storcli_path, "/call/vall", "show", "cc", "J", "nolog"],
            [storcli_path, "/call/vall", "show", "erase", "J", "nolog"],
            [storcli_path, "/call/vall", "show", "init", "J", "nolog"],
            [storcli_path, "/call/vall", "show", "migrate", "J", "nolog"],
        ],
        "event_logs": [storcli_path, "/call", "show", "events", "type=latest=5", "nolog", {"json_output": False}]  # fmt: skip
    }
    raw = {}
    for k, v in source.items():
        v = v if isinstance(v[0], list) else [v]
        fn = lambda f, e: f(*e) if not isinstance(e[-1], dict) else f(*e[:-1], **e[-1])
        raw[k] = [fn(storcli, e) for e in v]  # fmt: skip
        raw[k] = raw[k][0] if len(raw[k]) == 1 else raw[k]
    return raw


def read(options):
    sample_path = options.get("read_raw")
    abs_sample_path = os.path.abspath(sample_path)
    # Check if the sample file exists.
    if not os.path.isfile(abs_sample_path):
        sys.stderr.write(f"Cannot find {sample_path}.\n")
        exit(127)  # Note. OS return code for "command not found"
    # Check if the sample file is readable.
    if not os.access(abs_sample_path, os.R_OK):
        sys.stderr.write(f"Cannot read {sample_path}.\n")
        exit(126)  # Note. OS return code for "permission denied"

    with gzip.open(abs_sample_path, "rb") as stream:
        return json.loads(stream.read())


def dump(options):
    sample_path = options.get("dump_raw")
    abs_sample_path = os.path.abspath(sample_path)
    # Check if the sample file is writable.
    if not os.access(os.path.dirname(abs_sample_path), os.W_OK):
        sys.stderr.write(f"Cannot write {sample_path}.\n")
        exit(126)  # Note. OS return code for "permission denied"

    data = raw(options)
    abs_sample_path += "" if abs_sample_path.endswith(".gz") else ".gz"
    with gzip.open(abs_sample_path, "wb") as stream:
        stream.write(str.encode(json.dumps(data, separators=(",", ":"))))


def collect(raw):
    controllers = collect_controllers(raw.get("controllers"))
    enclosures = collect_enclosures(raw.get("enclosures"))
    virtual_drives = collect_virtual_drives(raw.get("virtual_drives"))
    physical_drives = collect_physical_drives(
        raw.get("controllers"),
        raw.get("virtual_drives"),
        raw.get("physical_drives"),
    )
    scheduled_tasks = collect_scheduled_tasks(raw.get("scheduled_tasks"))
    vd_operations = collect_virtual_drive_operations(raw.get("vd_operations"))
    event_logs = collect_event_logs(raw.get("event_logs"))
    return dictify(
        controllers, enclosures, virtual_drives, physical_drives,
        scheduled_tasks, vd_operations, event_logs
    )  # fmt: skip


def collect_controllers(ctrl_info):
    o = []
    for c in ctrl_info.get("Controllers"):
        if c.get("Command Status").get("Status") == "Success":
            controller = c.get("Command Status").get("Controller")
            r = c.get("Response Data")
            s = r.get("Basics")
            assert controller == s.get("Controller")
            model = s.get("Model")
            serial = s.get("Serial Number")
            status = r.get("Status").get("Controller Status")
            # fmt: off
            system_time = s.get("Current System Date/time")
            controller_time = s.get("Current Controller Date/Time")
            if system_time is not None and controller_time is not None:
                stime = format_time(system_time, False)
                ctime = format_time(controller_time, False)
                time_diff = abs(stime - ctime).seconds
            else: time_diff = None
            # fmt: on
            system_time = format_time(system_time)
            controller_time = format_time(controller_time)
            #
            s = r.get("Version")
            firmware = s.get("Firmware Version")
            driver = s.get("Driver Name")
            #
            s = r.get("HwCfg")
            memory = s.get("On Board Memory Size")
            temperature = s.get("ROC temperature(Degree Celsius)")
            temperature = temperature or s.get("ROC temperature(Degree Celcius)")
            frontend_ports = s.get("Front End Port Count")
            backend_ports = s.get("Backend Port Count")
            battery = s.get("BBU")
            alarm = s.get("Alarm")
            #
            o += [dictify(
                controller, driver, model, serial, firmware, status, memory,
                frontend_ports, backend_ports, temperature, battery, alarm,
                system_time, controller_time, time_diff
            )]  # fmt: skip
    return o


def collect_enclosures(encl_info):
    o = []
    for c in encl_info.get("Controllers"):
        if c.get("Command Status").get("Status") == "Success":
            controller = c.get("Command Status").get("Controller")
            r = c.get("Response Data")
            for k in r.keys():
                s = r.get(k).get("Information")
                enclosure = s.get("Device ID")
                enclosure_type = s.get("Enclosure Type")
                status = s.get("Status")
                device_type = s.get("Device Type")
                serial = s.get("Enclosure Serial Number", "N/A")
                serial = None if serial == "N/A" else serial

                s = r.get(k).get("Inquiry Data")
                vendor = s.get("Vendor Identification")
                product = s.get("Product Identification")
                revision = s.get("Product Revision Level", "N/A")
                revision = None if revision == "N/A" else revision

                s = r.get(k).get("Properties")[0]  # Why is it a list?
                slots = int(s.get("Slots"))
                physical_drives = int(s.get("PD"))
                #
                o += [dictify(
                    controller, enclosure, status, enclosure_type, device_type,
                    vendor, product, revision, slots, physical_drives,
                )]  # fmt: skip
    return o


def collect_virtual_drives(vd_info):
    o = []
    for c in vd_info.get("Controllers"):
        if c.get("Command Status").get("Status") == "Success":
            controller = c.get("Command Status").get("Controller")
            r = c.get("Response Data")
            for k in filter(lambda e: e.startswith(f"/c{controller}"), r.keys()):
                assert k.startswith(f"/c{controller}")
                virtual_drive = int(k[len(f"/c{controller}/v") :])

                s = r.get(f"/c{controller}/v{virtual_drive}")[0]  # Why is it a list?
                dg, vd = s.get("DG/VD", "-1/-1").split("/")
                dg, vd = int(dg), int(vd)
                assert virtual_drive == int(vd)
                type = s.get("TYPE")
                state = s.get("State")
                cache = s.get("Cache")
                size = s.get("Size").replace(" ", "")
                name = s.get("Name")
                cache_cade = s.get("Cac", "-")
                cache_cade = None if cache_cade == "-" else cache_cade
                drives = len(r.get(f"PDs for VD {vd}"))

                s = r.get(f"VD{vd} Properties")
                stripe = s.get("Strip Size").replace(" ", "")
                disk_cache = s.get("Disk Cache Policy")
                drive_name = s.get("OS Drive Name")
                per_span_count = s.get("Number of Drives Per Span")
                #
                o += [dictify(
                    controller, vd, dg, type, state, cache, size,
                    name, stripe, drives, disk_cache, drive_name, per_span_count
                )]  # fmt: skip
    return o


def collect_physical_drives(ctrl_info, vd_info, pd_info):
    m = {}  # Note. CID, DID (device) to VID (controller, virtual drive)
    for c in vd_info.get("Controllers"):
        if c.get("Command Status").get("Status") == "Success":
            controller = c.get("Command Status").get("Controller")
            r = c.get("Response Data")
            for k in filter(lambda e: e.startswith("PDs for VD "), r.keys()):
                vid = int(k[len("PDs for VD ") :].strip())
                for d in r.get(k):
                    m[f"{controller},{d.get('DID')}"] = vid
    n = {}  # Note. CID, DID (device) to DG, span, row (A.K.A topology)
    for c in ctrl_info.get("Controllers"):
        if c.get("Command Status").get("Status") == "Success":
            controller = c.get("Command Status").get("Controller")
            for k in c.get("Response Data").get("TOPOLOGY", []):
                if k.get("DID") != "-":
                    dg = int(k.get("DG"))
                    span = None if k.get("Arr") == "-" else int(k.get("Arr"))
                    row = None if k.get("Row") == "-" else int(k.get("Row"))
                    n[f"{controller},{k.get('DID')}"] = [dg, span, row]

    o = []
    for pd in pd_info:
        for c in pd.get("Controllers"):
            if c.get("Command Status").get("Status") == "Success":
                controller = c.get("Command Status").get("Controller")
                r = c.get("Response Data")
                for k in filter(lambda e: not e.endswith(" Information"), r.keys()):
                    assert k.startswith(f"Drive /c{controller}")
                    s = r.get(k)[0]  # Why is it a list?
                    eid_slt = s.get("EID:Slt").strip()
                    enclosure, slot = eid_slt.split(":")
                    enclosure = None if enclosure.strip() == "" else int(enclosure)
                    slot = int(slot)
                    did = s.get("DID")
                    vd = m.get(f"{controller},{did}")
                    state = s.get("State", "-")
                    state = None if state == "-" else state
                    size = s.get("Size").replace(" ", "")
                    interface = s.get("Intf")
                    media = s.get("Med")
                    spun = s.get("Sp", "-")
                    spun = None if spun == "-" else spun
                    type = s.get("Type", "-")
                    type = None if type == "-" else type
                    dg, span, row = n.get(f"{controller},{did}", [None, None, None])

                    s = r.get(f"{k} - Detailed Information").get(f"{k} State")
                    t = s.get("Shield Counter", "N/A")
                    shield_counter = None if t == "N/A" else int(t)
                    t = s.get("Media Error Count", "N/A")
                    media_error_count = None if t == "N/A" else int(t)
                    t = s.get("Other Error Count", "N/A")
                    other_error_count = None if t == "N/A" else int(t)
                    t = s.get("Predictive Failure Count", "N/A")
                    predictive_failure_count = None if t == "N/A" else int(t)
                    t = s.get("S.M.A.R.T alert flagged by drive", "N/A")  # Note. Dots in the string, do not use pydash.get.  # fmt: skip
                    smart_alert = None if t == "N/A" else t == "Yes"
                    t = s.get("Drive Temperature", "").strip().split("C")[0]
                    temperature = None if t == "" else int(t)

                    s = r.get(f"{k} - Detailed Information").get(f"{k} Device attributes")  # fmt: skip
                    manufacturer = s.get("Manufacturer Id").strip()
                    model = s.get("Model Number").strip()
                    serial = s.get("SN").strip()
                    t = s.get("Device Speed", "Unknown").replace("Gb/s", "")
                    drive_speed = None if t == "Unknown" else float(t)
                    link_speed = s.get("Link Speed", "Unknown").replace("Gb/s", "")
                    link_speed = None if t == "Unknown" else float(t)

                    s = r.get(f"{k} - Detailed Information").get(f"{k} Policies/Settings")  # fmt: skip
                    t = s.get("Commissioned Spare", "N/A")
                    commissioned_spare = None if t == "N/A" else t == "Yes"
                    t = s.get("Emergency Spare", "N/A")
                    emergency_spare = None if t == "N/A" else t == "Yes"
                    #
                    o += [dictify(
                        controller, enclosure, slot, did, vd, state, size, interface,
                        type, media, spun, shield_counter, media_error_count,
                        other_error_count, predictive_failure_count, smart_alert,
                        temperature, manufacturer, model, serial, drive_speed,
                        link_speed, dg, span, row, commissioned_spare, emergency_spare
                    )]  # fmt: skip
    return o


def collect_scheduled_tasks(task_info):
    o = []
    for c in task_info.get("Controllers"):
        if c.get("Command Status").get("Status") == "Success":
            controller = c.get("Command Status").get("Controller")
            r = c.get("Response Data").get("Controller Properties")
            r = {e.get("Ctrl_Prop"): e.get("Value") for e in r}
            if r:
                name = "Consistency check"
                mode = r.get("CC Operation Mode")
                state = r.get("CC Current State")
                next = format_time(r.get("CC Next Starttime"))
                delay = r.get("CC Execution Delay", "").replace(" hours", "")
                delay = None if delay == "" else int(delay)
                # Note. the number of virtual drives done by consistency check task
                done = r.get("CC Number of VD completed")
                done = None if done is None or state != "Active" else int(done)
                o += [dictify(controller, name, mode, state, next, delay, done)]
                #
                name = "Patrol read"
                mode = r.get("PR Mode")
                state = r.get("PR Current State")
                next = format_time(r.get("PR Next Start time"))
                delay = r.get("PR Execution Delay", "").replace(" hours", "")
                delay = None if delay == "" else int(delay)
                # Note. the number of physical drives done by patrol read task
                done = int(state[len("Active ") :]) if state.startswith("Active") else None  # fmt: skip
                o += [dictify(controller, name, mode, state, next, delay, done)]
    return o


def collect_virtual_drive_operations(op_info):
    o = []
    for op in op_info:
        for c in op.get("Controllers"):
            if c.get("Command Status").get("Status") == "Success":
                controller = c.get("Command Status").get("Controller")
                for r in c.get("Response Data").get("VD Operation Status"):
                    virtual_drive = r.get("VD")
                    name = r.get("Operation")
                    status = r.get("Status")
                    progress = r.get("Progress%", "-")
                    progress = None if progress == "-" else progress
                    time_left = r.get("Estimated Time Left", "-")
                    time_left = None if time_left == "-" else time_left
                    o += [dictify(
                        controller, virtual_drive, name, status, progress, time_left
                    )]  # fmt: skip
    return o


def collect_event_logs(log_info):
    event_severity = {
        "-1": "progress",  # Progress message. No user action is necessary.
        "0": "info",  # Informational message. No user action is necessary.
        "1": "warning",  # Some component might be close to a failure point.
        "2": "critical",  # A component has failed, but the system has not lost data.
        "3": "fatal",  # A component has failed, and data loss has occurred or will occur.
        "4": "fault",  # The I/O Unit faulted due to a catastrophic error.
    }
    event_kv = {
        "seqNum:": "sequence",
        "Time:": "time",
        "Seconds since last reboot:": "since",
        "Code:": "code",
        "Class:": "class",
        "Locale:": "locale",
        "Event Description:": "description",
    }
    event_keys = event_kv.keys()
    event_start_prefix = "seqNum: "
    controller_start_prefix = "CLI Version = "

    o = []
    parsed, event, state = [], {}, None
    for line in log_info.splitlines():
        line = line.strip()
        # fmt: off
        if line == "": continue  # skip empty lines
        event_start = line.startswith(event_start_prefix)
        controller_start = line.startswith(controller_start_prefix)
        if event_start or controller_start:
            if event: parsed += [event]
            event = {}
            state = "event" if event_start else "controller"
        if state == "event":
            k = next(filter(lambda e: line.startswith(e), event_keys), None)
            if k: event[event_kv.get(k)] = line[len(k) :].strip()
            if k == "Class:":
                event["severity"] = event_severity.get(event["class"], "unknown")
            elif k == "Time:":
                event["time"] = format_time(event["time"].replace("  ", " 0"))
        elif state == "controller" and line.startswith("Controller = "):
            controller = int(line[len("Controller = ") :].strip())
            o += [{"controller": controller, **e} for e in parsed]
            parsed = []
        # fmt: on
    return o


def translate(options, collected):
    def suffix_value(s, suffix):
        return None if s is None else f"{s}{suffix}"

    def translate_value(s, dictionary):
        return None if s is None else dictionary.get(s, s)

    def tag_value(s, dictionary):
        # fmt: off
        if s is None: return None
        if options.get("no_color"): return s
        tag = dictionary.get(s)
        return s if tag is None else f"<{tag}>{s}</{tag}>"

    copied = copy.deepcopy(collected)

    for d in copied.get("controllers"):
        d["temperature"] = suffix_value(d["temperature"], "C")
        d["status"] = translate_value(d["status"], {"Needs Attention": "Attention"})
        d["alarm"] = translate_value(d["alarm"], {"Disable": "Disabled"})
        d["status"] = tag_value(d["status"], {
            "Attention": "warning",
            "Degraded": "warning",
            "Failed": "fatal",
        })  # fmt: skip
        d["alarm"] = tag_value(d["alarm"], {"Disabled": "warning"})

    for d in copied.get("virtual_drives"):
        d["state"] = translate_value(d["state"], {"Dgrd": "Degraded", "Optl": "Optimal"})  # fmt: skip
        d["disk_cache"] = translate_value(d["disk_cache"], {"Disk's Default": "Default"})  # fmt: skip
        d["state"] = tag_value(d["state"], {"Degraded": "warning"})

    for d in copied.get("physical_drives"):
        d["temperature"] = suffix_value(d["temperature"], "C")
        d["drive_speed"] = suffix_value(d["drive_speed"], "Gbps")
        d["link_speed"] = suffix_value(d["link_speed"], "Gbps")
        d["state"] = translate_value(d["state"], {
            "Cpybck": "Copy back",
            "Offln": "Offline",
            "Onln": "Online",
            "Rbld": "Rebuild",
            "Sntze": "Sanitize",  # Wipe out disk data
            "CBShld": "CBS",  # copy back shielded
            "CFShld": "CFS",  # configured shielded
            "DHS": "DHS",  # dedicated hot spare
            "GHS": "GHS",  # global hos spare
            "HSPShld": "HSS",  # hot spare shielded
            "UBad": "UB",  # unconfigured bad
            "UBUnsp": "UBU",  # unconfigured bad unsupported
            "UGood": "UG",  # unconfigured good
            "UGShld": "UGS",  # unconfigured good shielded
            "UGUnsp": "UGU",  # Unconfigured good unsupported
        })  # fmt: skip
        d["spun"] = translate_value(d["spun"], {"U": "Up", "D": "Down"})
        d["type"] = translate_value(d["type"], {"F": "Foreign", "T": "Transition"})
        d["state"] = tag_value(d["state"], {
            "Rebuild": "info",
            "Offline": "warning",
            "Sanitize": "warning",
            "CBS": "info",
            "CFS": "info",
            "HSS": "info",
            "UGS": "info",
            "UB": "error",
            "UBU": "error",
        })  # fmt: skip
        d["type"] = translate_value(d["type"], {"Foreign": "info"})

    for d in copied.get("scheduled_tasks"):
        d["delay"] = suffix_value(d["delay"], " hours")
        d["mode"] = translate_value(d["mode"], {"Disable": "Disabled"})
        d["state"] = "Active" if d["state"].startswith("Active") else d["state"]
        d["mode"] = tag_value(d["mode"], {"Disabled": "warning"})
        d["state"] = tag_value(d["state"], {"Active": "info", "Paused": "info"})

    for d in copied.get("vd_operations"):
        d["name"] = translate_value(d["name"], {
            "BGI": "Background initialization",
            "CC": "Consistency check",
            "ERASE": "Erasure",
            "INIT": "Initialization",
            "Migrate": "Migration",
        })  # fmt: skip
        d["status"] = translate_value(d["status"], {
            "Not in progress": "Inactive",
            "In progress": "Active",
        })  # fmt: skip
        d["status"] = tag_value(d["status"], {"Active": "info"})

    for d in copied.get("event_logs"):
        d["severity"] = tag_value(d["severity"], {
            "info": "info",
            "warning": "warning",
            "critical": "error",
            "fatal": "fatal",
            "fault": "fatal",
        })  # fmt: skip

    return copied


def render(collected):
    SEPARATOR = "{{separator}}"

    def colorize(s, strip=False):
        tags = {
            "title": "\033[37m\033[1m",  # white
            "info": "\033[32m\033[1m",  # green
            "warning": "\033[33m\033[1m",  # yellow
            "error": "\033[31m\033[1m",  # red
            "fatal": "\033[31m\033[7m",  # red, inverse
            "reset": "\033[0m",  # reset
        }
        tagged = re.compile(r"<(\S+)>(.*)</\1>")
        matched = tagged.match(s)
        if matched and matched.groups:
            tag, body = matched.group(1), matched.group(2)
            open, close = ("", "") if strip else (tags.get(tag.lower(), ""), tags.get("reset", ""))  # fmt: skip
            # Note. s could be justified. So f"{tags.get(tag)}{body}{...}" will break column width.
            return s.replace(f"<{tag}>", open).replace(f"</{tag}>", close)
        return s

    def rows(data, cols):
        return [[c(e) if c(e) != "None" else "" for c in cols] for e in data]

    def maxwidth(headers, data):
        width = [len(colorize(e, True)) for e in headers]
        for d in data:
            if d != SEPARATOR:
                width = [max(width[i], len(colorize(e, True))) for i, e in enumerate(d)]
        return width

    def separate(data, key_index=0):
        separated = []
        for i in range(len(data)):
            if i >= 1 and data[i - 1][key_index] != data[i][key_index]:
                separated += [SEPARATOR]
            separated += [data[i]]
        return separated

    def tabulate(title, data, aligns, header, footer=None):
        justify = lambda e, i: (
            # Note. ljust and rjust won't be usable because of tags like "<info>...</info>".
            e + " " * (width[i] - len(colorize(e, True))) if aligns[i] == "l" else
            " " * (width[i] - len(colorize(e, True))) + e
        )  # fmt: skip
        format = lambda r: "|" + "".join(
            f" {colorize(justify(e, i))} |" for i, e in enumerate(r)
        )

        width = maxwidth(header, data)
        separator = "+" + "".join([f"-{'-'*e}-+" for e in width])
        empty = f"| No data available{' '*(len(separator) - 21)} |"
        rows = [separator if e == SEPARATOR else format(e) for e in data] if data else [empty]  # fmt: skip

        title = colorize(f"<title>{title.upper()}</title>")
        footer = [] if footer is None else footer
        return "\n".join(
            [title, separator, format(header), separator] + rows + [separator] + footer
        )

    o = []

    def render_time_diff(e):
        return "None" if e.get("time_diff") is None else f"{e.get('time_diff')} secs"

    title = "Controllers"
    data = rows(collected.get("controllers"), [
        lambda e: f"/c{e.get('controller')}",
        lambda e: f"{e.get('model')} ({e.get('serial')}, {e.get('firmware')})",
        lambda e: f"{e.get('status')}",
        lambda e: f"{e.get('memory')}",
        lambda e: f"{e.get('temperature')}",
        lambda e: f"{e.get('battery')}",
        lambda e: f"{e.get('alarm')}",
        lambda e: f"{e.get('system_time')}",
        render_time_diff,
    ])  # fmt: skip
    aligns = ["l", "l", "l", "r", "r", "l", "l", "l", "r"]
    header = ["CID", "Controller (serial, firmware)", "Status", "RAM", "Tpr", "Battery", "Alarm", "Clock (system)", "Time diff"]  # fmt: skip
    o += [tabulate(title, data, aligns, header)]

    title = "Enclosures"
    data = rows(collected.get("enclosures"), [
        lambda e: f"/c{e.get('controller')}/e{e.get('enclosure')}",
        lambda e: f"{e.get('vendor')} {e.get('product')} ({e.get('serial')}, {e.get('revision')})",  # fmt: skip
        lambda e: f"{e.get('status')}",
        lambda e: f"{e.get('device_type')}",
        lambda e: f"{e.get('slots')}",
        lambda e: f"{e.get('physical_drives')}",
    ])  # fmt: skip
    aligns = ["l", "l", "l", "l", "r", "r"]
    header = ["EID", "Enclosure (serial, rev.)", "Status", "Type", "#Slt", "#Drv"]
    o += [tabulate(title, data, aligns, header)]

    def render_cache(e):
        return e["cache"].replace("R", "R,").replace("C", ",C").replace("D", ",D")

    title = "Virtual drives"
    data = rows(collected.get("virtual_drives"), [
        lambda e: f"/c{e.get('controller')}/v{e.get('vd')}",
        lambda e: f"{e.get('type')}",
        lambda e: f"{e.get('state')}",
        lambda e: f"{e.get('size')}",
        lambda e: f"{e.get('drives')}",
        lambda e: f"{e.get('stripe')}",
        render_cache,
        lambda e: f"{e.get('disk_cache')}",
        lambda e: f"{e.get('cache_cade')}",
        lambda e: f"{e.get('name')}",
        lambda e: f"{e.get('drive_name')}",
    ])  # fmt: skip
    aligns = ["l", "l", "l", "r", "r", "r", "l", "l", "l", "l", "l"]
    header = ["VID", "Type", "Status", "Size", "#Drv", "StrpSz", "CacheFlg", "DskCache", "CacheCade", "Name", "Path"]  # fmt: skip
    footer = ['* R=Read Ahead Always,NR=No Read Ahead/WB=Write Back,AWB=Always Write Back,WT=Write Through/C=Cached IO,D=Direct IO']  # fmt: skip
    o += [tabulate(title, data, aligns, header, footer)]

    def render_vid(e):
        # fmt: off
        if e.get("vd") is None: return "None"
        return f"/c{e.get('controller')}/v{e.get('vd')}"

    def render_sid(e):
        # fmt: off
        if e.get("enclosure") is not None:
            return f"/c{e.get('controller')}/e{e.get('enclosure')}/s{e.get('slot')}"
        return "None" if e.get("slot") is None else f"/c{e.get('controller')}/s{e.get('slot')}"

    def render_model(e):
        prefix = "" if e.get("manufacturer") == "ATA" else f"{e.get('manufacturer')} "
        return f"{prefix}{e.get('model').replace('  ', ' ')} ({e.get('serial')})"

    def render_position(e):
        # fmt: off
        if e.get("dg") is None: return "None"
        if e.get("span") is None: return f"{str(e.get('dg')).rjust(2)}"
        return f"{str(e.get('dg')).rjust(2)}:{str(e.get('span')).rjust(2)}:{str(e.get('row')).rjust(2)}"

    def sort_data(d):
        # fmt: off
        s = sorted(d, key=lambda e: e[0])
        return list(filter(lambda e: e[0] != "", s)) + list(filter(lambda e: e[0] == "", s))

    title = "Physical drives"
    data = rows(collected.get("physical_drives"), [
        render_vid,
        render_sid,
        render_model,
        lambda e: f"{e.get('state')}",
        lambda e: f"{e.get('interface')}",
        lambda e: f"{e.get('media')}",
        lambda e: f"{e.get('size')}",
        lambda e: f"{e.get('spun')}",
        lambda e: f"{e.get('temperature')}",
        lambda e: f"{e.get('drive_speed')}",
        lambda e: f"{e.get('link_speed')}",
        lambda e: f"{e.get('did')}",
        render_position,
        lambda e: f"{e.get('type')}",
    ])  # fmt: skip
    # Note. Sort based on VID and separate based on VID
    data = separate(sort_data(data))
    aligns = ["l", "l", "l", "l", "l", "l", "r", "l", "r", "r", "r", "r", "l", "l"]
    header = ["VID", "SID", "Drive (serial)", "Status", "Intf", "Med", "Size", "Spun", "Tpr", "DiskSpd", "LinkSpd", "DID", "DG:SP:RW", "Type"]  # fmt: skip
    footer = [
        "* CBS=Copy Back Shielded,CFS=Configured Shielded,DHS=Dedicated Hot Spare,GHS=Global Hot Spare,HSS=Hot Spare Shielded,UB=Unconfigured Bad",
        "  UBU=Unconfigured Bad (Unsupported),UG=Unconfigured Good,UGS=Unconfigured Good Shielded,UGU=Unconfigured Good (Unsupported)",
    ]
    o += [tabulate(title, data, aligns, header, footer)]

    title = "Controller tasks"
    data = rows(collected.get("scheduled_tasks"), [
        lambda e: f"/c{e.get('controller')}",
        lambda e: f"{e.get('name')}",
        lambda e: f"{e.get('mode')}",
        lambda e: f"{e.get('delay')}",
        lambda e: f"{e.get('next')}",
        lambda e: f"{e.get('state')}",
        lambda e: f"{e.get('done')}",  # TODO: Write "done" count only if state is active.
    ])  # fmt: skip
    # Note. Separate based on CID.
    data = separate(data)
    aligns = ["l", "l", "l", "r", "l", "l", "l"]
    header = ["CID", "Task", "Mode", "Delay", "Next run (controller)", "Status", "Progress"]  # fmt: skip
    o += [tabulate(title, data, aligns, header)]

    def render_progress(e):
        # fmt: skip
        if e.get("status") == "Inactive":
            return "None"
        return f"{e.get('progress')}%, {format_duration(e.get('time_left'))} left"

    title = "Virtual drive operations"
    data = rows(collected.get("vd_operations"), [
        lambda e: f"/c{e.get('controller')}/v{e.get('virtual_drive')}",
        lambda e: f"{e.get('name')}",
        lambda e: f"{e.get('status')}",
        render_progress,
    ])  # fmt: skip
    # Note. Sort based on VID and filter out inactive operations.
    data = list(filter(lambda e: e[2] != "Inactive", sort_data(data)))
    aligns = ["l", "l", "l", "l"]
    header = ["VID", "Operation", "Status", "Progress"]
    o += [tabulate(title, data, aligns, header)]

    def render_time(e):
        return e.get("time") if e.get("time") else f"{e.get('since')} secs since reboot"

    def render_description(e, maxlen=90):
        # fmt: off
        if len(e.get("description")) <= maxlen: return e.get("description")
        return e.get('description')[:maxlen - 3] + '...'

    title = "Event logs"
    data = rows(collected.get("event_logs"), [
        lambda e: f"/c{e.get('controller')}",
        lambda e: f"{int(e.get('sequence'), 0)}",
        render_time,
        render_description,
        lambda e: f"{e.get('severity').upper()}",
    ])  # fmt: skip
    # Note. Separate based on CID.
    data = separate(data)
    aligns = ["l", "r", "l", "l", "l"]
    header = ["CID", "Index", "Event time", "Log", "Severity"]
    # @see https://techdocs.broadcom.com/content/dam/broadcom/techdocs/data-center-solutions/tools/generated-pdfs/12Gbs-MegaRAID-Tri-Mode-Software.pdf
    footer = ["* CRITICAL=error without data loss,FATAL=error with possible data loss,FAULT=catastrophic hardware failure"]  # fmt: skip
    o += [tabulate(title, data, aligns, header, footer)]

    return "\n".join(o)


def parse():
    parser = argparse.ArgumentParser(
        description="Print MegaRAID controller and drive information",
        formatter_class=lambda prog: argparse.HelpFormatter(
            prog, max_help_position=100
        ),
    )
    # fmt: off
    parser.add_argument("--storcli-path", metavar="<string>", type=str, default="/opt/MegaRAID/storcli/storcli64", help="specify storcli executable path")
    parser.add_argument("--format", choices=['humans', 'json'], default="humans", help="specify the output format (default: humans)")
    parser.add_argument("--no-color", action="store_true", default=False, help="print in monochrome")
    parser.add_argument("--dump-raw", metavar="<string>", type=str, help="specify file path to dump raw JSON data (for debugging)")
    parser.add_argument("--read-raw", metavar="<string>", type=str, help="specify file path to read raw JSON data (for debugging)")
    parser.add_argument("--version", action="version", version="%(prog)s " + __version__)
    # fmt: on
    return parser


if __name__ == "__main__":
    options = vars(parse().parse_args())
    if options.get("dump_raw"):
        dump(options)
        exit(0)
    data = read(options) if options.get("read_raw") else raw(options)
    collected = collect(data)
    if options.get("format") == "json":
        sys.stdout.write(json.dumps(collected) + "\n")
    else:
        translated = translate(options, collected)
        rendered = render(translated)
        sys.stdout.write(rendered + "\n")
    sys.stdout.flush()
